<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        if(!Auth::user()) {
            return view('auth.login');
        } else {
            return redirect('/users');
        }
    }
    public function displayUsers()
    {
        $users = User::orderBy('id', 'ASC')->latest()->get();
        return view('users.index', compact('users'));
    }

    public function editUsers($id)
    {
        $singleUser = User::findOrFail($id);
        return view('users.edit', compact('singleUser'));
    }

    public function saveUser(Request $request)
    {
        $input = $request->all();
        User::create($input);
        return redirect('/users');
    }
}
