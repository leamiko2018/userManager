<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_1 = new User();
        $user_1->name = 'Test User 1';
        $user_1->email = 'testUser1@gmail.com';
        $user_1->password = Hash::make('testingUser1Password!');
        $user_1->role_id = '2';
        $user_1->save();

        $user_2 = new User();
        $user_2->name = 'Test User 2';
        $user_2->email = 'testUser2@gmail.com';
        $user_2->password = Hash::make('testingUser2Password!');
        $user_2->role_id = '2';
        $user_2->save();

        $user_3 = new User();
        $user_3->name = 'Test User 3';
        $user_3->email = 'testUser3@gmail.com';
        $user_3->password = Hash::make('testingUser3Password!');
        $user_3->role_id = '2';
        $user_3->save();

        $user_4 = new User();
        $user_4->name = 'Test User 4';
        $user_4->email = 'testUser4@gmail.com';
        $user_4->password = Hash::make('testingUser4Password!');
        $user_4->role_id = '2';
        $user_4->save();

        $admin_1 = new User();
        $admin_1->name = 'Test Admin 1';
        $admin_1->email = 'testAdmin1@gmail.com';
        $admin_1->password = Hash::make('testingAdmin1Password!');
        $admin_1->role_id = '1';
        $admin_1->save();

        $admin_2 = new User();
        $admin_2->name = 'Test Admin 2';
        $admin_2->email = 'testAdmin2@gmail.com';
        $admin_2->password = Hash::make('testingAdmin2Password!');
        $admin_2->role_id = '1';
        $admin_2->save();

        $admin_3 = new User();
        $admin_3->name = 'Test Admin 3';
        $admin_3->email = 'testAdmin3@gmail.com';
        $admin_3->password = Hash::make('testingAdmin3Password!');
        $admin_3->role_id = '1';
        $admin_3->save();


    }
}
