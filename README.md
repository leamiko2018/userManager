# User Management System

## Setup
* [Get Docker] (https://www.docker.com/get-docker)
* [Install Composer Globally] (https://getcomposer.org/doc/00-intro.md)
* [Install Node/NPM Globally] (https://nodejs.org/en/)
* Install PHP 7.0+ on your local machine globally
* Make the following addition to your `/etc/hosts` file:

```
127.0.0.1 			mysql
```


## Clone Project
* `git clone git@gitlab.com:quincarter/userManager.git`
* Make a copy of the `.env.example` and name it `.env`
* Run the following command in the project root to get started!     
    * `npm i && npm run setup:fresh`
    * If a migration error occurrs on setup, run this command manually:
        * `npm run migrate:fresh`

App will be served [here] (http://localhost:10000)
