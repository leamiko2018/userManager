@extends('layouts.app')

@section('content')
<display-users-component
        :display-users="{{ json_encode($users) }}"
        :is-admin="{{ Auth::user()->role_id }}">
</display-users-component>
@endsection
